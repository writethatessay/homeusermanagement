import React, { useState, useEffect } from 'react';
import { SafeAreaView, Text, TextInput, View, TouchableOpacity, AppState, TouchableWithoutFeedback } from 'react-native';
import { css } from '@emotion/native';
import Icon from 'react-native-vector-icons/Ionicons';
import * as Animatable from 'react-native-animatable';

import { login } from "../includes/auth";
import { useAppState } from "../contexts/AppContext";

const LoginScreen = (props) => {
    const { navigation } = props;
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [effectOnLoginButton, setEffectOnLoginButton] = useState(Math.random());
    const [effectOnImage, setEffectOnImage] = useState(Math.random());
    const [effectOnImage2, setEffectOnImage2] = useState(Math.random());
    const [effectOnImage3, setEffectOnImage3] = useState(Math.random());
    const [effectOnText1, setEffectOnText1] = useState(Math.random());
    const [effectOnText2, setEffectOnText2] = useState(Math.random());
    const [appState,] = useAppState();

    const handleLoginClick = () => {
        setEffectOnLoginButton(Math.random());
        if (email === '' || password === '') return;
        console.log('"login"', "login");
        login(email, password).then(() => {
            console.log('appState', appState);
            console.log('appState.getUser', appState.getUser);
            appState.getUser().then((user) => {
                console.log('user ##############', user);
                // redirectAfterSignIn(user);
                setEmail('');
                setPassword('');
                navigation.navigate('Overview');
            });
        })
    }

    const onPressMenuHandler = () => {

    }

    const _handleAppStateChange = (nextAppState) => {
        if (nextAppState === "active") {
            setEffectOnImage(Math.random());
            setEffectOnImage2(Math.random());
            setEffectOnImage3(Math.random());
        }
    }

    useEffect(() => {
        AppState.addEventListener('change', _handleAppStateChange);

        return () => {
            AppState.removeEventListener('change', _handleAppStateChange);
        }
    }, []);

    return (
        <>
            <SafeAreaView style={loginContainerStyle}>
                <View style={headerStyle}>
                    <Animatable.Image
                        useNativeDriver
                        duration={4000}
                        animation={'tada'}
                        key={effectOnImage}
                        style={imageStyle}
                        source={require('../images/writers-toolbox-logo-single-line.png')}
                    />
                    <TouchableOpacity onPress={onPressMenuHandler}>
                        <Icon
                            name="menu"
                            color="#FFFFFF"
                            size={26}
                        />
                    </TouchableOpacity>
                </View>

                <View style={bodyStyle}>
                    <View style={titleContainerStyle}>
                        <Text style={titleTextStyle}>Log in</Text>
                    </View>

                    <View style={socialLoginContainerStyle}>
                        <Animatable.View
                            useNativeDriver
                            duration={800}
                            animation={'fadeInLeft'}
                            key={effectOnImage2}
                            style={googleLoginStyle}
                        >
                            <Icon
                                name="logo-google"
                                color="#FFFFFF"
                                size={26}
                            />
                            <View style={twoLinesTextStyle}>
                                <Text style={socialLoginButtonTextStyle}>Log in with</Text>
                                <Text style={socialLoginButtonTextStyle}>Google</Text>
                            </View>
                        </Animatable.View>
                        <Animatable.View
                            useNativeDriver
                            duration={800}
                            animation={'fadeInRight'}
                            key={effectOnImage3}
                            style={facebookLoginStyle}
                        >
                            <Icon
                                name="logo-facebook"
                                color="#FFFFFF"
                                size={26}
                            />
                            <View style={twoLinesTextStyle}>
                                <Text style={socialLoginButtonTextStyle}>Log in with</Text>
                                <Text style={socialLoginButtonTextStyle}>Facebook</Text>
                            </View>
                        </Animatable.View>
                    </View>

                    <View style={delimiterContainerStyle}>
                        <View style={delimiterStyle} />
                        <Text>or</Text>
                        <View style={delimiterStyle} />
                    </View>

                    <View style={loginInputContainerStyle}>
                        <Text style={inputTitleTextStyle}>Email / Username</Text>
                        <TextInput
                            style={inputStyle}
                            placeholder={"Enter email address or username"}
                            value={email}
                            onChangeText={setEmail}
                        />
                    </View>

                    <View style={loginInputContainerStyle}>
                        <Text style={inputTitleTextStyle}>Password</Text>
                        <TextInput
                            style={inputStyle}
                            placeholder={'Password'}
                            value={password}
                            onChangeText={setPassword}
                            secureTextEntry
                        />
                    </View>

                    <View style={descriptionContainerStyle}>
                        <Text style={descriptionTextStyle}>Don't have an account? Please </Text>
                        <Animatable.View
                            useNativeDriver
                            duration={800}
                            animation={'rubberBand'}
                            key={effectOnText1}
                        >
                            <TouchableWithoutFeedback onPress={() => setEffectOnText1(Math.random())}><Text style={linkTextStyle}>sign up.</Text></TouchableWithoutFeedback>
                        </Animatable.View >
                    </View>

                    <View style={descriptionContainerStyle}>
                        <Text style={descriptionTextStyle}>Forgot your password? Please </Text>
                        <Animatable.View
                            useNativeDriver
                            duration={800}
                            animation={'rubberBand'}
                            key={effectOnText2}
                        >
                            <TouchableWithoutFeedback onPress={() => setEffectOnText2(Math.random())}><Text style={linkTextStyle}>click here.</Text></TouchableWithoutFeedback>
                        </Animatable.View >
                    </View>

                    <View style={submitButtonContainerStyle}>
                        <TouchableWithoutFeedback onPress={handleLoginClick}>
                            <Animatable.View
                                useNativeDriver
                                duration={800}
                                animation={'rubberBand'}
                                key={effectOnLoginButton}
                                style={submitButtonStyle}>
                                <Text style={submitButtonTextStyle}>
                                    Log in
                                </Text>
                            </Animatable.View >
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </SafeAreaView>
        </>
    );
};

const loginContainerStyle = css`
    width: 100%;
    height: 100%;
`;

const headerStyle = css`
    width: 100%;
    height: 60px;
    background: #1d8bdb;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    position: relative;
    padding: 0 20px;
`;

const imageStyle = css`
    width: 70%;
    height: 20px;
`;

const bodyStyle = css`
    padding: 40px 20px;
`;

const titleContainerStyle = css`
    width: 100%;
    height: 40px;
    padding-left: 20px;
    background-color: #d1d4d6;
    display: flex;
    justify-content: center;
    margin-bottom: 10px;
`;

const titleTextStyle = css`
    font-size: 22px;
`;

const socialLoginContainerStyle = css`
    width: 100%;
    height: 50px;
    margin-bottom: 10px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

const googleLoginStyle = css`
    width: 47%;
    height: 100%;
    background-color: #a5331c;
    border: 0px;
    border-radius: 3px;
    box-shadow: rgba(0,0,0,0.5) 0px 1px 2px;
    padding: 0px 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const facebookLoginStyle = css`
    width: 47%;
    height: 100%;
    background-color: #3b5998;
    border: 0px;
    border-radius: 3px;
    box-shadow: rgba(0,0,0,0.5) 0px 1px 2px;
    padding: 0px 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const twoLinesTextStyle = css`
    display: flex;
    margin-left: 10px;
`;

const socialLoginButtonTextStyle = css`
    color: #ffffff;
    font-weight: 500;
`;

const delimiterContainerStyle = css`
    width: 100%;
    margin: 5px 0 20px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

const delimiterStyle = css`
    width: 45%;
    border-bottom-width: 1px;
    border-color: grey;
`;

const loginInputContainerStyle = css`
    
`;

const inputTitleTextStyle = css`
    font-size: 16px;
    font-weight: 300;
`;

const inputStyle = css`
    border-width: 0.5px;
    border-radius: 5px;
    width: 100%;
    height: 50px;
    margin: 5px 0 20px;
    padding: 10px;
    font-weight: 300;
`;

const descriptionContainerStyle = css`
    display: flex;
    flex-direction: row;
    margin-bottom: 10px;
`;

const descriptionTextStyle = css`
    font-size: 13px;
    font-weight: 300;
`;

const linkTextStyle = css`
    font-size: 13px;
    font-weight: 300;
    color: #1D8BDB;
`;

const submitButtonContainerStyle = css`
    display: flex;
    align-items: flex-end;
`;

const submitButtonStyle = css`
    min-width: 35%;
    height: 40px;
    background-color: #1d8bdb;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
`;

const submitButtonTextStyle = css`
    color: #ffffff;
    font-size: 16px;
    font-weight: 600;
`;

export default LoginScreen;
