import React, { useState, useEffect } from 'react';
import { SafeAreaView, Text, Image, View, TouchableOpacity, AppState } from 'react-native';
import { css } from '@emotion/native';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Ionicons';
import pluralize from 'pluralize';
import { times } from "lodash";
import MenuDrawer from 'react-native-side-drawer';
import Hamburger from 'react-native-animated-hamburger';

import { useQuery, gql } from "@apollo/client";
import UnderlinedTitle from '../components/UnderlinedTitle';
import { useAppState } from '../contexts/AppContext';
import useCurrentPlan from '../hooks/useCurrentPlan';
import useCurrentPaymentMethod from '../hooks/useCurrentPaymentMethod';

const OverviewScreen = () => {
    const [appState] = useAppState();
    const { currentPlan, numberOfUsers } = useCurrentPlan();
    const { currentPaymentMethod, loading } = useCurrentPaymentMethod();
    const { data: profileData } = useQuery(GET_HOMEUSER_STUDENTS_QUERY);

    const user = appState?.user;
    const subscription = appState?.user?.Subscription;
    const stripeCard = currentPaymentMethod?.StripeCard;
    const profiles = profileData?.getHomeUserStudents ?? [];

    const [effectOnLoginButton, setEffectOnLoginButton] = useState(Math.random());
    const [effectOnImage, setEffectOnImage] = useState(Math.random());
    const [effectOnImage2, setEffectOnImage2] = useState(Math.random());
    const [effectOnImage3, setEffectOnImage3] = useState(Math.random());
    const [effectOnText1, setEffectOnText1] = useState(Math.random());
    const [effectOnText2, setEffectOnText2] = useState(Math.random());
    const [isDrawer, setIsDrawer] = useState(false);

    const onAddUserClick = () => {
        //history.push("/manage/user-profiles/add");
        //navigation.navigate('user-profiles');
    };

    const onPressMenu = () => {
        setIsDrawer(!isDrawer);
    }

    const onPressProfile = () => {

    }

    const _handleAppStateChange = (nextAppState) => {
        if (nextAppState === "active") {
            setEffectOnImage(Math.random());
            setEffectOnImage2(Math.random());
            setEffectOnImage3(Math.random());
        }
    }

    useEffect(() => {
        AppState.addEventListener('change', _handleAppStateChange);

        return () => {
            AppState.removeEventListener('change', _handleAppStateChange);
        }
    }, []);

    const drawerContent = () => {
        return (
            <View style={drawerStyle}>

            </View>
        );
    };

    return (
        <>
            <SafeAreaView style={overviewStyle}>
                <MenuDrawer
                    open={isDrawer}
                    drawerContent={drawerContent()}
                    drawerPercentage={45}
                    animationTime={250}
                    overlay={true}
                    opacity={1}
                >
                </MenuDrawer>

                <View style={headerStyle}>
                    <View style={headerIconStyle}>
                        <Hamburger color={'white'} type="spinCross" active={isDrawer} onPress={onPressMenu} />
                    </View>
                    <TouchableOpacity style={profileContainerStyle} onPress={onPressProfile}>
                        <View style={headerIconStyle}>
                            <Icon
                                name="person-outline"
                                color="#FFFFFF"
                                size={20}
                            />
                        </View>
                        <Text style={profileTextContainerStyle}>
                            Profile
                        </Text>
                        <Icon
                            name="chevron-down-sharp"
                            color="#1D8BDB"
                            size={20}
                        />
                    </TouchableOpacity>
                </View>

                <View style={contentStyle}>
                    <View style={titleContainerStyle}>
                        <Animatable.Text
                            useNativeDriver
                            duration={2500}
                            animation={'swing'}
                            key={effectOnImage}
                            style={titleTextStyle}
                        >
                            Account Overview
                        </Animatable.Text>
                    </View>

                    <View style={accountHolderDetailsContaionerStyle}>
                        <UnderlinedTitle>
                            ACCOUNT HOLDER DETAILS
                        </UnderlinedTitle>

                        <View style={itemStyle}>
                            <Animatable.Text
                                useNativeDriver
                                duration={2500}
                                animation={'bounceInLeft'}
                                delay={300}
                                style={itemTextStyle}
                            >
                                {user?.FirstName} {user?.Surname}
                            </Animatable.Text>
                            <TouchableOpacity style={itemIconStyle} onPress={onPressMenu}>
                                <Icon
                                    name="pencil-sharp"
                                    color="#FFFFFF"
                                    size={14}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={itemStyle}>
                            <Animatable.Text
                                useNativeDriver
                                duration={2500}
                                animation={'bounceInLeft'}
                                delay={600}
                                style={itemTextStyle}>
                                {user?.Email}
                            </Animatable.Text>
                            <TouchableOpacity style={itemIconStyle} onPress={onPressMenu}>
                                <Icon
                                    name="pencil-sharp"
                                    color="#FFFFFF"
                                    size={14}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={itemStyle}>
                            <Animatable.Text
                                useNativeDriver
                                duration={2500}
                                animation={'bounceInLeft'}
                                delay={900}
                                style={itemTextStyle}>Password: **********
                                </Animatable.Text>
                            <TouchableOpacity style={itemIconStyle} onPress={onPressMenu}>
                                <Icon
                                    name="pencil-sharp"
                                    color="#FFFFFF"
                                    size={14}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={subscriptionDetailsContentsStyle}>
                        <UnderlinedTitle>
                            SUBSCRIPTION DETAILS
                        </UnderlinedTitle>

                        <View style={itemStyle}>
                            <View>
                                {user?.Subscription && (
                                    <Animatable.View
                                        useNativeDriver
                                        duration={2500}
                                        animation={'bounceInLeft'}
                                        delay={1200}
                                        style={itemTextStyle}>
                                        <Text
                                            style={itemTextStyle}>
                                            {user.Subscription.ProductName} {currentPlan?.Interval === "month" ? " - Monthly" : " - Annually"}
                                        </Text>
                                        {numberOfUsers && (
                                            <Text
                                                style={itemTextStyle}>
                                                {pluralize('user', numberOfUsers, true)}
                                            </Text>
                                        )}
                                    </Animatable.View >
                                )}
                            </View>
                            <TouchableOpacity style={itemIconStyle} onPress={onPressMenu}>
                                <Icon
                                    name="eye-sharp"
                                    color="#FFFFFF"
                                    size={14}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={paymentMEthodContaionerStyle}>
                        <UnderlinedTitle>
                            PAYMENT METHOD
                        </UnderlinedTitle>

                        <View style={itemStyle}>
                            <Animatable.View
                                useNativeDriver
                                duration={2500}
                                animation={'bounceInLeft'}
                                delay={1500}
                                style={cardStyle}>
                                {stripeCard && (
                                    <>
                                        <View style={imageWrapperStyle}>
                                            <Image style={imageStyle} source={require('../images/visa.png')} />
                                        </View>
                                        <Text style={cardTextStyle}>********{stripeCard.last4}</Text>
                                    </>
                                )}
                            </Animatable.View>
                            <TouchableOpacity style={itemIconStyle} onPress={onPressMenu}>
                                <Icon
                                    name="eye-sharp"
                                    color="#FFFFFF"
                                    size={14}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={toolboxProfilesContaionerStyle}>
                        <UnderlinedTitle>
                            WRITER'S TOOLBOX PROFILES
                        </UnderlinedTitle>

                        <Animatable.View
                            useNativeDriver
                            duration={2500}
                            animation={'bounceInLeft'}
                            delay={1800}
                            style={toolboxProfilesContentsStyle}>
                            {profiles.map((profile) => (
                                <View style={profileIconWrapperStyle} key={profile.id}>
                                    <View style={profileIconStyle}>
                                        <Icon
                                            name="person-outline"
                                            color="#FFFFFF"
                                            size={20}
                                        />
                                    </View>
                                    <Text style={profileTextStyle}>{profile.FirstName}</Text>
                                </View>
                            ))}
                            {times(numberOfUsers - profiles.length).map(num => (
                                <TouchableOpacity key={num} style={addProfileStyle} onPress={onPressMenu}>
                                    <View style={plusIconWrapperStyle}>
                                        <Icon
                                            name="add"
                                            color="#FFFFFF"
                                            size={35}
                                        />
                                    </View>
                                    <Text style={profileTextStyle}>
                                        Add Profile
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </Animatable.View>
                    </View>
                </View>
            </SafeAreaView>
        </>
    );
};

const GET_HOMEUSER_STUDENTS_QUERY = gql`
    query getHomeUserStudents {
        getHomeUserStudents {
            id
            FirstName
        }
    }
`;

const overviewStyle = css`
    width: 100%;
    height: 100%;
    display: flex;
`;

const headerStyle = css`
    width: 100%;
    height: 10%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding: 23px 18px;
`;

const headerIconStyle = css`
    width: 40px;
    height: 40px;
    border-radius: 100px;
    background-color: #1D8BDB;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left: 1px;
    padding-top: 1px;
`;

const profileContainerStyle = css`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const profileTextContainerStyle = css`
    margin-left: 20px;
    color: #1D8BDB;
`;

const contentStyle = css`
    padding: 20px 0;
    width: 340px;
    height: 90%;
    display: flex;
    margin: auto;
    z-index: -1;
`;

const titleContainerStyle = css`
    height:40px;
`;

const titleTextStyle = css`
    font-size: 34px;
    font-weight: 700;
    color: #0869B2
`;

const accountHolderDetailsContaionerStyle = css`
    height: 164px;
    margin-bottom: 40px;
`;

const itemStyle = css`
    height: 46px;
    padding: 10px 0;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

const itemIconStyle = css`
    width: 26px;
    height: 26px;
    border-radius: 100px;
    background-color: #1D8BDB;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left: 2px;
    padding-top: 1px;
`;

const itemTextStyle = css`
    font-size: 14px;
    font-weight: 700;
    color: #066AB2;
    position: relative;
    line-height: 20px;
    margin-top: 3px;
`;

const cardTextStyle = css`
    margin-left: 10px;
    font-size: 13px;
`;

const imageWrapperStyle = css`
    width: 40px;
    height: 24px;
    background: #dedede;
    padding: 6px;
    border-radius: 3px;
`;

const imageStyle = css`
    flex:1;
    width:null;
    height:null;
    resize-mode: contain;
`;

const subscriptionDetailsContentsStyle = css`
    height: 72px;
    margin-bottom: 40px;
`;

const paymentMEthodContaionerStyle = css`
    height: 72px;
    margin-bottom: 40px;
`;

const cardStyle = css`
    display: flex;
    flex-direction: row;
`;

const toolboxProfilesContaionerStyle = css`
    height: 134px;
    margin-bottom: 40px;
`;

const toolboxProfilesContentsStyle = css`
    display: flex;
    flex-direction: row;
`;

const addProfileStyle = css`
    display: flex;
`;

const plusIconWrapperStyle = css`
    width: 60px;
    height: 60px;
    border-radius: 100px;
    background-color: #63C973;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left: 4px;
    margin-left: 10px;
`;

const profileTextStyle = css`
    font-weight: 300;
    color: #555555;
    line-height: 20px;
    padding-top: 8px;
    text-align: center;
`;

const profileIconWrapperStyle = css`
    display: flex;
    align-items: center;
    padding-left: 4px;
    margin-left: 10px;
`;

const profileIconStyle = css`
    width: 60px;
    height: 60px;
    border-radius: 100px;
    background-color: #1D8BDB;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const drawerStyle = css`
    width: 100%;
    height: 100%;
    background: #1D8BDB;
`;

export default OverviewScreen;
