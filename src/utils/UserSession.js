import React, { useCallback, useEffect, useRef, useState } from 'react';
import gql from 'graphql-tag';
import { useAppState } from "../contexts/AppContext";
import { useLazyQuery } from "@apollo/client";
import { clearToken, getToken, getTokenParam } from "../includes/auth";
import useToken from "../hooks/useToken";

const UserSession = () => {
    const [appState, setAppState] = useAppState();
    const [noUser, setNoUser] = useState(false);
    const user = appState?.user;
    const tokenUserId = useToken();
    const resolveRef = useRef();
    const rejectRef = useRef();

    const onCompleted = (data, a, b) => {
        if (resolveRef && resolveRef.current) resolveRef.current(data?.getHomeUser);
    }
    const onError = (error) => {
        if (rejectRef && rejectRef.current) rejectRef.current(error);
    }

    const [getData, { called, data, loading, refetch }] = useLazyQuery(GET_HOME_USER_QUERY, {
        onCompleted: onCompleted,
        onError: onError
    });

    const getUser = useCallback(() => {
        return new Promise((resolve, reject) => {
            if (resolveRef) resolveRef.current = resolve;
            if (rejectRef) rejectRef.current = reject;
            getData();
        });
    }, [getData]);

    const refetchUser = useCallback(() => {
        return refetch
            ? refetch()
                .then(response => response?.data?.getHomeUser)
                .catch(response => response?.errors)
            : getUser();
    }, [refetch, getUser]);

    /**
     * Clears the user.
     */
    const clearUser = useCallback(() => {
        return new Promise((resolve, reject) => {
            setAppState({
                user: null,
                authorized: false,
            })
            resolve();
        });
    }, [setAppState]);

    // if no user, or the token has changed then refetch the user
    useEffect(() => {
        if (tokenUserId && (!user || parseInt(user.id) !== tokenUserId)) {
            setNoUser(false);
            getData();
        } else {
            setNoUser(true);
        }
    }, [clearUser, user, getData, tokenUserId]);

    // set user if fetched data changes
    useEffect(() => {
        const userData = data?.getHomeUser;
        const authorized = !!userData;
        if (!userData && (called && !loading)) {
            clearToken();
        }
        setAppState({
            authorized,
            hasUserBeenLoaded: !!data || (called && !loading) || noUser,
            clearUser: clearUser,
            getUser: getUser,
            refetchUser: refetchUser,
            user: userData,
        });
    }, [clearUser, data, getUser, refetch, setAppState, called, loading, noUser, refetchUser]);

    return (<></>);
};

const GET_HOME_USER_QUERY = gql`
    query getHomeUser {
        getHomeUser {
            id
            FirstName
            Surname
            Email
            IsEmailVerified
            IsSocialConfirmed
            HasViewedGettingStarted
            LastLoginType
            StripeCustomerID
            EmailVerification {
                IsValid
            }
            Subscription {
                id
                CurrentPeriodEnd
                NiceCurrentPeriodEnd
                IsValid
                Quantity
                PaymentIntent
                Price
                ProductName
                SubscriptionID
                CancelStatus
                CancelScheduleRequestedAt
                PaymentStatus
                FirstPaymentStatus
                BillingHistory {
                    id
                    InvoiceID
                    Currency
                    Amount
                    Card
                    CardType
                    Created
                    ReceiptURL
                }
            }
        }
    }
`;

export default UserSession;
