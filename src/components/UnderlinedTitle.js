import React from 'react';
import { Text, View } from 'react-native';
import { css } from '@emotion/native';

const UnderlinedTitle = ({children}) => {

    return (
        <>
            <View style={underlinedTitleStyle}>
                <Text style={textStyle}>
                    {children}
                </Text>
            </View>
        </>
    );
};

const underlinedTitleStyle = css`
    width: 100%;
    display: flex;
    border-bottom-color: #066AB2;
    border-bottom-width: 1;
    padding-bottom: 6px;
    margin: 35px 0 5px;
`;

const textStyle = css`
    color: #066AB2;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 400;
`;

export default UnderlinedTitle;
