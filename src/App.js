import React from 'react';
import { SafeAreaView, Text, } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from "./screens/LoginScreen";
import OverviewScreen from "./screens/OverviewScreen";

import apolloClient from './includes/apollo';
import AppContext from "./contexts/AppContext";
import useSetState from "./hooks/useSetState";
import { ApolloProvider } from "@apollo/client";
import UserSession from "./utils/UserSession";

const Stack = createStackNavigator();

const DEFAULT_APP_STATE = {
    hasUserBeenLoaded: false,
    user: null,
    country: {
        code: '',
        codeFromIP: '',
        name: '',
    },
    plans: null,
};

const App = () => {
    const appState = useSetState(DEFAULT_APP_STATE);
    return (
        <ApolloProvider client={apolloClient}>
            <AppContext.Provider value={appState}>

                <UserSession />

                <NavigationContainer>
                    <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Login">
                        <Stack.Screen name="Login" component={LoginScreen} />
                        <Stack.Screen name="Overview" component={OverviewScreen} />
                    </Stack.Navigator>
                </NavigationContainer>

            </AppContext.Provider>
        </ApolloProvider>
    );
};

export default App;
