import { useQuery, gql } from "@apollo/client";

const useCurrentPaymentMethod = () => {
    const { data, loading } = useQuery(GET_HOMEUSER_CURRENT_PAYMENT_METHOD_QUERY)
    return { currentPaymentMethod: data?.getCurrentPaymentMethod, loading }
}

const GET_HOMEUSER_CURRENT_PAYMENT_METHOD_QUERY = gql`
    query getCurrentPaymentMethod {
        getCurrentPaymentMethod {
            id
            name
            phone
            StripeBillingAddress {
                city
                country
                line1
                line2
                postalCode
                state
            }
            StripeCard {
                brand
                country
                last4
            }
        }
    }
`

export default useCurrentPaymentMethod
