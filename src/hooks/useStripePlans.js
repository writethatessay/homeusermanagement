import { useAppState } from "../contexts/AppContext";
import { useLazyQuery, gql } from "@apollo/client";
import { useEffect } from "react";

export const TRIAL_DAYS = 30;

const useStripePlans = () => {
    const [ appState, setAppState ] = useAppState();
    const [ runQuery, { data } ] = useLazyQuery(GET_PLANS);

    const countryCode = appState.country.code;

    // Wait for appState to have a country code before executing query based on country code.
    useEffect(
        () => {
            if (countryCode !== "") {
                runQuery({});
            }
        },
        [countryCode, runQuery]
    );

    useEffect(
        () => {
            if (data) {
                const sortedData = data.getStripePlans.sort(plan => plan.Interval === 'month' ? -1 : 1);
                setAppState({ plans: sortedData });
            }
        },
        [data, setAppState]
    );

    return appState.plans;
};

const GET_PLANS = gql`
    query getStripePlans {
        getStripePlans {
            id
            Currency
            NiceCurrency
            Interval
            Tiers {
                id
                UnitAmount
                UpTo
            }
        }
    }
`;

export default useStripePlans;
