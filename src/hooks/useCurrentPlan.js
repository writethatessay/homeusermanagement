import { useEffect } from "react";
import { useAppState } from "../contexts/AppContext";
import useStripePlans from "./useStripePlans";
import useSetState from "./useSetState";
import { getNiceTotalPriceFromTier } from "../includes/stripePlansHelpers";

const useCurrentPlan = () => {
    const [appState] = useAppState()
    const stripePlans = useStripePlans()
    const [state, setState] = useSetState({
        currentPlan: null,
        totalPrice: 0,
        numberOfUsers: 1,
    })

    useEffect(
        () => {
            if (!stripePlans || !appState?.user || !appState?.user?.Subscription) {
                return
            }

            const currentPlanID = appState.user.Subscription.Price;
            const numberOfUsers = appState.user.Subscription.Quantity;
            const currentPlan = currentPlanID && stripePlans.find((plan) => plan.id === currentPlanID)
            setState({ currentPlan: currentPlan, numberOfUsers: numberOfUsers })
        },
        [stripePlans, appState, setState]
    )

    useEffect(
        () => {
            if (state.currentPlan) {
                setState({
                    totalPrice: getNiceTotalPriceFromTier(state.currentPlan, state.numberOfUsers.toString())
                })
            }
        },
        [state.currentPlan, state.numberOfUsers, setState]
    )

    return state
}

export default useCurrentPlan
