import { useCallback, useState } from 'react';
import { persist, retrieve } from "../includes/localStorage";

/**
 * Manage state similar to setState on class components. Optionally persist state across loads. If local storage is used then
 * default state will be loaded from local storage if present.
 *
 * @param {object}      defaultState
 * @param {string|null} persistKey
 *
 * @returns {[unknown, (...args: any[]) => any]}
 */
const useSetState = (defaultState, persistKey = null) => {

    // if state is persisted, check for stored state, if found we merge with default state in case default state
    // has changed since state was stored
    if (persistKey) {
        const storedState = retrieve(persistKey);

        if (storedState) {
            defaultState = { ...defaultState, ...storedState};
        }
    }

    const [state, setState] = useState(defaultState);

    const updateState = useCallback((newState, merge = true) => {
        setState(prevState => {
            // if we have a function, pass previous state
            const updatedState = typeof newState === 'function' ? newState(prevState) : newState;
            let stateUpdate = null;

            // merge new state with previous state
            if (merge) {
                stateUpdate = {
                    ...prevState,
                    ...updatedState,
                };

                // if merge is disabled, merge with the default state
            } else {
                stateUpdate = {
                    ...defaultState,
                    ...updatedState,
                };
            }

            if (persistKey) {
                persist(persistKey, stateUpdate);
            }

            return stateUpdate;
        });
    }, [true]);

    return [state, updateState];
};

export default useSetState;
