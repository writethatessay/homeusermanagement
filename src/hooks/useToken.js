import { getToken, getTokenParam } from "../includes/auth";
import { useCallback, useEffect, useState } from "react";

const useToken = () => {
    const [tokenUserId, setTokenUserId] = useState();

    // useEffect(() => {
    //     getToken().then(token => {
    //         const tokenUserId = token ? parseInt(getTokenParam(token.token, 'user')) : null;
    //         setTokenUserId(tokenUserId);
    //     });
    // }, []);

    const fetchMyAPI = useCallback(async () => {
        const token = await getToken();
        const tokenUserId = token ? parseInt(getTokenParam(token.token, 'user')) : null;
        setTokenUserId(tokenUserId);
    }, []);

    useEffect(() => {
        fetchMyAPI()
    }, [fetchMyAPI])

    return tokenUserId;
}

export default useToken;