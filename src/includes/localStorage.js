import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 * Retrieve an item from local storage.  Call function based on decode option.
 *
 * @param key
 * @param value
 * @param decode
 *
 * @return {*}
 */
export const retrieve = async (key, value = '', decode = true) => {
    if (storageExists()) {
        return decode ? await retrieveObject(key, value) : await retrieveValue(key);
    }
};

/**
 * Retrieve a json encoded item from local storage.
 *
 * @param key
 * @param value
 *
 * @return {*}
 */
export const retrieveObject = async (key, value = '') => {
    let ls = null;
    if (storageExists()) {
        try {
            let item = await AsyncStorage.getItem(key);

            ls = JSON.parse(item) || {};
        } catch (e) {
            /*Ignore*/
        }
    }

    // if we want a nested value, check for the value
    if (value) {
        return ls && ls.hasOwnProperty(value) ? ls[value] : null;
    }

    return ls;
};

/**
 * Retrieve a raw value from local storage.
 *
 * @param key
 *
 * @return {*}
 */
export const retrieveValue = async (key) => {
    if (storageExists()) {
        return await AsyncStorage.getItem(key);
    }
};

/**
 * Persists an item to local storage.  Call function based on encode option.
 *
 * @param key
 * @param value
 * @param encode
 *
 * @returns {*}
 */
export const persist = (key, value, encode = true) => {
    if (storageExists()) {
        encode ? persistObject(key, value) : persistValue(key, value);
    }
};

/**
 * Persists an item as a stringified value to local storage.
 *
 * @param key
 * @param value
 *
 * @returns {*}
 */
export const persistObject = async (key, value) => {
    if (storageExists()) {
        await AsyncStorage.setItem(key, JSON.stringify(value));
    }
};

/**
 * Persists a raw value in local storage.
 *
 * @param key
 * @param value
 *
 * @returns {*}
 */
export const persistValue = async (key, value) => {
    if (storageExists()) {
        await AsyncStorage.setItem(key, value);
    }
};

/**
 * Remove an item from local storage.
 *
 * @param key
 */
export const removeItem = async (key) => {
    if (storageExists()) {
        await AsyncStorage.removeItem(key);
    }
};


/**
 * Check if localStorage is available.
 *
 * @returns {boolean}
 */
export const storageExists = () => !!AsyncStorage;
