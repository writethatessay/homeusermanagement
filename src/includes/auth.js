import {
    AUTH_BEARER_PREFIX,
    AUTH_HEADER,
    AUTH_HEADER_ORIGIN,
    AUTH_REFRESH_LEEWAY,
    AUTH_STORAGE_KEY
} from "./constants";
import jwt_decode from 'jwt-decode';
import moment from 'moment';
import * as storage from "./localStorage";
//import { API_BASE_URL } from "@env";

const API_BASE_URL = 'http://127.0.0.1';
/**
 * Save the token to storage.
 *
 * @param token
 */
export const setToken = (token) => {
    const decoded = jwt_decode(token);
    const expiresIn = decoded.exp - decoded.iat;
    const expiresAt = moment().add(expiresIn, 's');

    const tokenData = {
        token,
        expiresAt,
    };

    storage.persist(AUTH_STORAGE_KEY, tokenData);

    return tokenData;
};

/**
 * Extracts a param from the token.
 *
 * @param token
 * @param param
 */
export const getTokenParam = (tokenData, param) => {
    console.log('tokenData', tokenData);
    console.log('param', param);
    const decoded = jwt_decode(tokenData);
    console.log('decoded', decoded);

    if (decoded && decoded.hasOwnProperty(param)) {
        console.log('decoded[param]', decoded[param]);
        return decoded[param];
    }


    console.log('"return null"', "return null");

    return null;
};


/**
 * Clear the token from storage.
 */
export const clearToken = () => storage.removeItem(AUTH_STORAGE_KEY);

export const getToken = async () => {
    return await storage.retrieve(AUTH_STORAGE_KEY);
};

/**
 * Add the bearer token to headers.
 *
 * @param tokenData
 * @param headers
 * @returns {*}
 */
export const addTokenHeader = (tokenData, headers) => {
    if (tokenData && tokenData.token) {
        headers[AUTH_HEADER] = `${AUTH_BEARER_PREFIX}${tokenData.token}`;
    }
    headers[AUTH_HEADER_ORIGIN] = 'http://wtbox.com';

    return headers;
};

/**
 * Should the token be refreshed. Checks if current time is between the after the refresh time. Expiry should
 * also be checked with hasExpired.
 *
 * @param tokenData
 * @returns {boolean|boolean}
 */
export const shouldTokenRefresh = (tokenData) => {
    // can't refresh if we don't have a token
    if (!tokenData || !tokenData.expiresAt) return false;

    const expires = moment(tokenData.expiresAt);
    const refreshTime = expires.clone().subtract(AUTH_REFRESH_LEEWAY, 's');
    const now = moment();

    return now.isSameOrAfter(refreshTime);
};

/**
 * Has the token has expired.
 *
 * @param tokenData
 * @returns {boolean}
 */
export const hasTokenExpired = (tokenData) => {
    if (!tokenData || !tokenData.expiresAt) return true;

    const expires = moment(tokenData.expiresAt);
    const now = moment();

    return now.isAfter(expires);
};

/**
 * Extract the token from the headers and store it.
 *
 * @param headers
 * @returns {null|*}
 */
export const updateTokenFromHeaders = async (headers) => {
    const header = headers.get(AUTH_HEADER);

    if (header && header.indexOf(AUTH_BEARER_PREFIX) === 0) {
        const token = header.replace(AUTH_BEARER_PREFIX, '');
        const existingTokenData = await getToken();

        // only update if it has changed
        if (!existingTokenData || token !== existingTokenData.token) {
            return setToken(token);
        }
    }

    return null;
};

/**
 * Refresh the token. It sends original token and receives an updated token in the headers.
 *
 * @param tokenData
 * @returns {Promise<null>}
 */
export const refreshToken = async (tokenData) => {
    const headers = addTokenHeader(tokenData, {});

    const response = await fetch(`${API_BASE_URL}/auth/refresh`, {
        method: 'POST',
        headers,
        body: JSON.stringify({withoutSession: true}),
        credentials: 'omit'
    });

    const data = await response.json();

    if (data.status) {
        return updateTokenFromHeaders(response.headers);
    }

    throw new Error('Unable to refresh token');
};

/**
 * Login with username/password.
 *
 * @param {string} email
 * @param {string} password
 *
 * @returns {Promise<void>}
 */
export const login = async (email, password) => {
    const response = await fetch(`${API_BASE_URL}/auth/login`, {
        credentials: 'omit',
        method: 'POST',
        body: JSON.stringify({ email, password, withoutSession: true, domain: 'wtbox.com' }),
        headers: {
            'Content-Type': 'application/json',
            'Origin': 'http://wtbox.com',
        },
    });

    const data = await response.json();
    // handle a redirect for logging into wrong site
    if (data.redirect) {
        window.location = data.redirect;
        // successful login, store token
    } else if (data.status) {
        setToken(data.token);
        // error with a message
    } else if (data.message) {
        //throw new LoginError(data.message);
        throw new Error(data.message);
        // specific html error message
    } else if (data.htmlMessage) {
        //throw new LoginError(data.htmlMessage, 'html', data.link);
        throw new Error(data.message);
    }
};

/**
 * Login with social profile.
 *
 * @param {string} email
 * @param {string} token
 * @param {string} type
 *
 * @returns {Promise<void>}
 */
export const socialLogin = async (email, token, type) => {
    const body = {email, token, type, withoutSession: true, domain: 'wtbox.com'};

    const response = await fetch(`${API_BASE_URL}/auth/login`, {
        credentials: 'include',
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
            'Content-Type': 'application/json',
            'Origin': 'http://wtbox.com',
        }
    });

    const data = await response.json();

    // handle a redirect for logging into wrong site
    if (data.redirect) {
        window.location = data.redirect;
    } else if (data.status) {
        setToken(data.token);
    } else if (data.message) {
        throw new Error(data.message);
    } else if (data.htmlMessage) {
        throw new Error(data.htmlMessage, 'html', data.link);
    }
};

export type SocialLoginType = 'google' | 'facebook';

export const isSocialLoginUser = (user) => {
    return user?.LastLoginType === 'google' || user?.LastLoginType === 'facebook';
};

/**
 * Need to confirm the social
 *
 * @param {any} user
 *
 * @returns {boolean}
 */
export const needSocialConfirmation = (user) => {
    return !!(isSocialLoginUser(user) && !user?.IsSocialConfirmed);
};

/**
 * Need to verify the email
 *
 * @param {any} user
 *
 * @returns {boolean}
 */
export const needEmailVerification = (user) => {
    return !!(user?.LastLoginType === 'password' && !user?.IsEmailVerified);
};
