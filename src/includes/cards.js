import React from "react";
import { ReactComponent as visaCardIcon } from "../../images/cards/visa.svg";
import { ReactComponent as masterCardIcon } from "../../images/cards/master.svg";
import { ReactComponent as americanExpressIcon } from "../../images/cards/american-express.svg";

export const AmericanExpressIcon = americanExpressIcon;
export const VisaCardIcon = visaCardIcon;
export const MasterCardIcon = masterCardIcon;


// type brand: 'visa' | 'mastercard' | 'amex' | 'discover' | 'diners' | 'jcb' | 'unionpay' | 'unknown';
export type CardType = string | 'visa' | 'mastercard' | 'amex';

type CardMapType = {
    [c in CardType]: React.ComponentType
}

const cardMap: CardMapType = {
    'visa': VisaCardIcon,
    'mastercard': MasterCardIcon,
    'amex': AmericanExpressIcon,
};

export const getCardIcon = (cardType: CardType) => {
    if (!cardType) return;
    return cardMap[cardType.toLowerCase()];
}
