// For an explanation on policy details, see:
// https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-config-options-fetchPolicy
// and
// https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-config-options-errorPolicy
import { InMemoryCache, HttpLink, ApolloClient, ApolloLink } from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import { setContext } from "@apollo/client/link/context";
import * as authService from "./auth";
//import { API_BASE_URL } from "@env";

const API_BASE_URL = 'http://127.0.0.1';
const defaultOptions = {
    watchQuery: {
        fetchPolicy: 'network-only',
        errorPolicy: 'none',
    },
    query: {
        fetchPolicy: 'network-only',
        errorPolicy: 'none',
    },
};

//Global error handle for when a user is logged out.  Redirects to login page.
const errorHandlerLink = onError((errorHandler) => {
    const networkError = errorHandler.networkError;

    console.log('networkError', networkError);

    if (networkError && networkError.statusCode === 401) {

        let event = new Event("authError");
        window.dispatchEvent(event);

        // capture response information for network errors
    } else if (networkError && networkError.response) {
        let body = networkError.bodyText || '';

        // limit length, max size of sentry logging request is 100kB
        if (body.length >= 5000) {
            body = body.substring(0, 5000);
        }
    }
});

/**
 * Adds bearer token to apollo requests.
 *
 * @type {ApolloLink}
 */
const authMiddleware = setContext(async (operation) => {
    let token = await authService.getToken();

    if (token && token.token) {

        if (!authService.hasTokenExpired(token)) {
            // refresh token if it needs refreshing
            if (authService.shouldTokenRefresh(token)) {
                try {
                    token = await authService.refreshToken(token);
                } catch (e) {
                    // do something
                }
            }

            console.log('"header"', authService.addTokenHeader(token, {}));
            // add the authorization to the headers
            return {
                headers: authService.addTokenHeader(token, {}),
                credentials: 'omit'
            };
        }
    }

    return {
        credentials: 'omit'
    };
    // if no token we include credentials so session can be used, if there is a valid session a token
    // will be returned in the headers
    // return withCredentials;
});

const authAfterware = new ApolloLink((operation, forward) => {
    return forward(operation).map(response => {
        const { response: { headers } } = operation.getContext();
        if (headers) {
            authService.updateTokenFromHeaders(headers);
        }

        return response;
    });
});

const createClient = () => new ApolloClient({
    // By default, this client will send queries to the
    //  `/graphql` endpoint on the same host
    // Pass the configuration option { uri: YOUR_GRAPHQL_API_URL } to the `HttpLink` to connect
    // to a different host
    link: ApolloLink.from(
        [
            authMiddleware,
            authAfterware,
            errorHandlerLink,
            new HttpLink({uri: `${API_BASE_URL}/graphql`}),
        ]
    ),
    cache: new InMemoryCache(),
    defaultOptions: defaultOptions
});

export default createClient();
