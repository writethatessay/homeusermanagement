export const getNiceUnitAmount = (unitAmount) => unitAmount / 100;

export const getNicePriceFromTier = (plan, numberOfUsers) => {
    const upToValue = parseInt(numberOfUsers) === 1 ? 1 : null; // UpTo value will be 1 for 1 account, or null for multiple
    const unitAmount = plan.Tiers.filter( tier => tier.UpTo === upToValue)[0].UnitAmount;
    return getNiceUnitAmount(unitAmount);
};

export const getNiceTotalPriceFromTier = (plan, numberOfUsers) => {
    const nicePrice = getNicePriceFromTier(plan, numberOfUsers)
    return (nicePrice * parseInt(numberOfUsers)).toFixed(2)
};

export const getPlanByInterval = (plans, interval) => {
    return plans.filter( plan => plan.Interval === interval)[0];
};

export const getPriceByInterval = (plan, numberOfUsers) => {
    const price = getNicePriceFromTier(plan, numberOfUsers);

    return plan.Interval === 'month' ? price : price / 12;
};

export default {
    getNiceUnitAmount,
    getNicePriceFromTier,
    getNiceTotalPriceFromTier,
    getPlanByInterval,
    getPriceByInterval,
};
